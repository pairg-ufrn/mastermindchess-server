#include "gameDAO.h"

void GameDAO::savePlayers(vector<Player> playerlist){
    ofstream output {configurationDAO.getDataDirectory() + "elos"};

    for (auto& player : playerlist){
        output << player.getID() << ";";
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++){
                output  << player.getElo(i, j)       << ";"
                        << player.getVictories(i, j) << ";"
                        << player.getDefeats(i, j)   << ";"
                        << player.getDraws(i, j)     << ";";
            }
        output << "\n";
    }
    output << std::flush;
}


vector<Player> GameDAO::loadPlayers( ){
    ifstream input   {configurationDAO.getDataDirectory() + "elos"};
    vector<Player> playerlist;
    string id, tempNick, tempElo, tempVic, tempDef, tempDraw;

    int i = 0;

    if(!Utility::isEmpty(input)){

        while(getline(input, id, ';')){

            if(id != "\n" and id != ""){

                playerlist.emplace_back();

                playerlist[i].setID(Utility::StringToNumber(id));

                for(int k = 0; k < 3; k++){
                    for(int j = 0; j < 3; j++){
                        getline(input, tempElo,    ';');
                        getline(input, tempVic,    ';');
                        getline(input, tempDef,    ';');
                        getline(input, tempDraw,   ';');
                        playerlist[i].setElo        (Utility::StringToNumber(tempElo),  k, j);
                        playerlist[i].setVictories  (Utility::StringToNumber(tempVic),  k, j);
                        playerlist[i].setDefeats    (Utility::StringToNumber(tempDef),  k, j);
                        playerlist[i].setDraws      (Utility::StringToNumber(tempDraw), k, j);
                    }
                }
                i++;
                if(input.peek() == EOF or input.peek() == 0) break;
            }
        }
    }
    return playerlist;
}

int GameDAO::loadMatches(){
    ifstream matchInput {configurationDAO.getDataDirectory() + "matches"};

    string aux;
    int numberOfMatches = 0;

    if(!Utility::isEmpty(matchInput)){
        while(getline(matchInput, aux)){
            numberOfMatches++;
        }
    }
    return numberOfMatches;
}

void GameDAO::logMatch(Match* match){
    ofstream output {configurationDAO.getLogDirectory() + "mastermind_" + Utility::getDatestamp("") + ".log", ios::app};
    string ranked = (match->getRanked()? "ranked" : "casual");
    string ispublic = (match->getPublic()? "public" : "private");
    string whitename = match->getWhite()->getNickname();
    string blackname = match->getBlack()->getNickname();
    string gamemode = Utility::toString(match->getGameMode());
    string gametime = Utility::toString(match->getGameTime());
    output  <<  Utility::getTimestamp()  << " [" << ranked << "]"
            << "[" << ispublic << "]"
            << "[" << gamemode << "]"
            << "[" << gametime << "]"
            << "[" << whitename<< "]"
            << "[" << blackname<< "]\n";
}

void GameDAO::saveMatch(Match* match){

    ofstream output {configurationDAO.getDataDirectory() + "matches", ios::app};

    output  << match->getID()               << ";"
            << match->getWhite()->getID()   << ";"
            << match->getBlack()->getID()   << ";"
            << match->getWinner()->getID()  << ";"
            << match->getEloWon()           << ";"
            << match->getLoser()->getID()   << ";"
            << match->getEloLost()          << ";"
            << match->getEloLost()          << ";"
            << Utility::getDatestamp(true,true)     << ";"
            << match->getGameMode()         << ";"
            << match->getGameTime()         << ";\n";
    output << std::flush;
}


void GameDAO::saveMatchInPGNFormat(Match* match){
    string PGN;
    ofstream output {configurationDAO.getDataDirectory() + "pgns/match_" + Utility::toString(match->getID()+1) + ".pgn", ios::app};

    PGN =   "[Event MasterMind Chess Ranked Match]\n[Date "  + Utility::getDatestamp(true,true) +
            "]\n[White "  + match->getWhite()->getNickname()  + "]\n[Black " + match->getBlack()->getNickname() +
            "]\n[Winner " + match->getWinner()->getNickname() + "]\n\n";

    output << PGN;
    output << match->getMatchHistory();
    output << std::flush;
}
