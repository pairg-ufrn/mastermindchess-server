#include "serverDAO.h"

void ServerDAO::logHistory (string action, string playerName) {
    ofstream output {configurationDAO.getLogDirectory() + "daemon/daemon_" + Utility::getDatestamp("") + ".log", ios::app};
    output  <<  Utility::getTimestamp()  << " "
            << playerName       << " "
            << action           << "\n";
    output  << std::flush;
}

void ServerDAO::logHistory (string action, string playerName, int connectionID) {
    ofstream output {configurationDAO.getLogDirectory() + "daemon/daemon_" + Utility::getDatestamp("") + ".log", ios::app};
    output  <<  Utility::getTimestamp()  << " "
            <<  playerName      << " "
            <<  action          << " "
            <<  connectionHandler.connections[connectionID]->TcpSocket::getRemoteAddress().toString() << "\n";
    output << std::flush;
}

void ServerDAO::logPacket (sf::Packet packet, string inout, int connectionID) {
    string inorout = (inout == "in"? "Receiving" : "Sending");
    string tofrom = (inout == "in"? "from" : "to");
    int pktID;
    packet >> pktID >> pktID;

    ofstream output {configurationDAO.getLogDirectory() + "packets/" + "packets_" + Utility::getDatestamp("") + "_" + inout + ".log", ios::app};
    output  <<  Utility::getTimestamp()  << " "
            <<  inorout << " packet " << pktID << " " << tofrom << " "
            << connectionHandler.connections[connectionID]->TcpSocket::getRemoteAddress().toString() << "\n";
    output  << std::flush;
}

void ServerDAO::logCrash(std::string cause){
    std::string filepath = configurationDAO.getLogDirectory();
    ofstream output {filepath + "crashes/crash_" + Utility::getDatestamp("") + "_" + Utility::getTimestamp("") + ".err", ios::app};
    output  << Utility::getTimestamp()  << " - MMC Server Crash Report" << endl
            << "Cause: " << cause << "\n"
            << "Last sended packet: "   << Utility::toString(connectionHandler.getLastSendedPacketID()) << "(sended to " << Utility::toString(connectionHandler.lastSendedPacketReceiver) << ")\n"
            << "Last received packet: " << Utility::toString(connectionHandler.getLastSendedPacketID()) << "(received from " << Utility::toString(connectionHandler.lastReceivedPacketSender) << ")\n"
            << "Online users: " << Utility::toString(connectionHandler.getOnlineUsers()) << "\n"
            << "Last user to log in: " << Utility::toString(connectionHandler.lastLogin) << "\n"
            << "Last user to log out: " << Utility::toString(connectionHandler.lastLogout) << "\n";
}
