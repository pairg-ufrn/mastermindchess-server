#ifndef _GAMEFACADE
#define _GAMEFACADE
#include "dataType.h"
#include "gameControl.h"
#include "match.h"
#include "player.h"

class Match;

class Player;

class GameFacade{
    public:
        static void handleRequest(sf::Packet packet, int pktID, int fromUser);
        static bool setPlayerStatus(int ID, statusID status);
        static Player* getPlayer(int playerID);
        static Player* getPlayer(std::string nickname);
        static vector<Match> getMatches();
        static int getNumberOfMatches();
        static int getNumberOfPlayers();
        static void registerPlayer (int ID);
};

#endif // _GAMEFACADE
