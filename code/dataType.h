#ifndef _DATA
#define _DATA

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <sstream>

using namespace std;

const std::string GAME_VERSION = "0.1.15";

                    //0     1     2       3       4         5            6          7          8            9           10     11
enum class packetID {None, Name, Move, Connect, Login, LoginResponse, GameEnd, Disconnect, Register, RegisterResponse, Chat, GameRequest,
                    Response, GameOptions, Elo, MatchLog, Check, Checkmate, KeepAlive, WrongVersion, PlayerList, UserList, GameEndTimeOut,
                    GiveUp, MatchList, WatchRequest, WatchState, ExitQueue, BoardStatus, GameInvite, InviteResponse, FischerPieceOrder, SpectatorChat,
                    MatchHistory, numberWatching, InviteRejection, Broadcast, AlreadyConnected};

enum class masterPacketID {None, Login, LoginResponse, Disconnect, MatchList};

enum class statusID {offline, online, playing, watching};

enum class gameType {Classic = 0, Fischer, Capa};



#endif // _DATA
