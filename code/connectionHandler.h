#ifndef _NETHAND
#define _NETHAND

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include <memory>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <vector>

#include "dataType.h"
#include "networkFacade.h"
#include "configurationDAO.h"

#ifndef connectionHandler
#define connectionHandler ConnectionHandler::getInstance()
#endif

class ConnectionHandler{
    public:

        sf::Packet packet;
        int playerID;

        void run();
        static ConnectionHandler& getInstance();
        ConnectionHandler(){};

        vector<unique_ptr<sf::TcpSocket>> connections;

        int getLastReceivedPacketID();
        int getLastReceivedPacketSender();

        int getLastSendedPacketID();
        int getLastSendedPacketReceiver();

        int getOnlineUsers();

        sf::Packet lastSendedPacket;

        int lastReceivedPacketSender;
        int lastSendedPacketReceiver;

        int lastLogin;
        int lastLogout;


    private:
        static ConnectionHandler* instance;

        ConnectionHandler(ConnectionHandler const&);
        void operator=(ConnectionHandler const&);

        int port;

        sf::TcpListener                   listener;

        sf::Packet lastReceivedPacket;


        int onlineUsers;

        void acceptNewConnections();
        void receivePacket();
        void loadConfiguration();


};


#endif // _NETHAND
