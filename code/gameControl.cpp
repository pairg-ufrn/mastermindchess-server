#include "gameControl.h"


GameControl& GameControl::getInstance(){
    static GameControl instance;
    return instance;
}

GameControl::GameControl(){
    numberOfMatches = GameDAO::loadMatches();
    Players = GameDAO::loadPlayers();
    rankManager = new RankManager();
    rankManager->loadRanks(Players);
}


sf::Packet& operator<<(sf::Packet& packet, std::vector<std::vector<std::basic_string<char>>> vet){
    int i, j;
    for(i = 0; i < vet.size(); i++){
        for(j = 0; j < vet[i].size(); j++){
            packet << vet[i][j];
        }
    }
}


void GameControl::registerPlayer(int ID){
    Player player(ID);
    Players.push_back(player);
    //rankManager.updatePlayersReference(Players);
    //rankManager.addNewPlayerToRank(&Players[Players.size()-1]);
    rankManager->loadRanks(Players);
    GameDAO::savePlayers(Players);

    return;
}

vector<Player> GameControl::getPlayers(){
    return Players;
}

vector<Match> GameControl::getMatches(){
    return Matches;
}

vector <Player*> GameControl::getRank(int gameMode, int gameTime){
    return rankManager->getRank(gameMode, gameTime);
}


void GameControl::calculateELO(Match* match, Player* winner, Player* loser){
    int gameMode = match->getGameMode();
    int gameTime = match->getGameTime()/900;

    int winnerELO = winner->getElo( gameMode, gameTime);
    int loserELO = loser->getElo( gameMode, gameTime);

    int eloWon = getELOScore(winnerELO, loserELO, 1);
    int eloLost = getELOScore(loserELO, winnerELO, 0);

    match->setEloWon(eloWon);
    match->setEloLost(eloLost);

    winner->setElo(winnerELO + eloWon,  gameMode, gameTime);
    loser->setElo (loserELO  + eloLost, gameMode, gameTime);

    winner->raiseVictories(gameMode, gameTime);
    loser->raiseDefeats(gameMode, gameTime);
}

int GameControl::getELOScore(int Ra, int Rb, double Sa){ //Elo from player 1 and player 2, and result of the match
    int K = 15;
    double Ea = 1/(1 + pow(10, ( (Rb - Ra)/400 ) ));
    return (int)(K*(Sa - Ea));
}


void GameControl::sendInviteRequest(int playerID, string name, int gameMode, int gameTime, bool isPublic){

    int i;

    Player* player = getPlayer(playerID);
    Player* invitedPlayer = getPlayer(name);
    Utility::buildPacketHeader(playerID, packetID::GameInvite, packet);

    packet << player->getNickname() << gameMode << gameTime << isPublic;

    Utility::sendPacket(UserFacade::getUser(invitedPlayer->getID())->getConnectionID(), packet);

}


void GameControl::sendInviteResponse(int playerID, string invitee, int gameMode, int gameTime, bool isPublic, bool response){

    if(response)
        processRequest(playerID, getPlayer(invitee)->getID(), static_cast<gameType>(gameMode), gameTime, isPublic, 0);
    else
        sendInviteRejection(playerID, getPlayer(invitee)->getID(), invitee);
}

void GameControl::sendInviteRejection(int playerID, int toPlayer, string user){
    Utility::buildPacketHeader(playerID, packetID::InviteRejection, packet);
    packet << user;

    Utility::sendPacket(UserFacade::getUser(toPlayer)->getConnectionID(), packet);
}


void GameControl::sendWatchState(int playerID, int matchID){
    int i, j;

    Utility::buildPacketHeader(playerID, packetID::WatchState, packet);

    Match* match = getMatch(matchID);
    if(match != nullptr){
       packet << match->getWhite()->getNickname() << match->getBlack()->getNickname()
               << match->getWhite()->getElo( match->getGameMode(), match->getGameTime()/900 )
               << match->getBlack()->getElo( match->getGameMode(), match->getGameTime()/900 );
        for(i = 0; i < 8; i++){
            for(j = 0; j < 10; j++){
                packet << match->getBoard(i, j);
            }
        }

        packet << match->getGameMode();

        Utility::sendPacket(UserFacade::getUser( getPlayer(playerID)->getID() )->getConnectionID(), packet);
    }

}

void GameControl::sendNumberWatching(int playerID, int number){
    Utility::buildPacketHeader(playerID, packetID::numberWatching, packet);

    packet << number;
    Utility::sendPacket(UserFacade::getUser( getPlayer(playerID)->getID() )->getConnectionID(), packet);
}

void GameControl::sendFischerPiecesOrder(int playerID, string newBoard[][10]){
    int k, p;
    Utility::buildPacketHeader(playerID, packetID::FischerPieceOrder, packet);

    for(k = 0; k < 8; k++){
        for(p = 0; p < 10; p++){
            packet << newBoard[k][p];
        }
    }
    Utility::sendPacket(UserFacade::getUser( getPlayer(playerID)->getOpponentID() )->getConnectionID(), packet);
}


void GameControl::sendMove(int playerID, int i, int j, int iP, int jP, bool check, string moveLog){

    Utility::buildPacketHeader(playerID, packetID::Move, packet);
    packet << i << j << iP << jP << check << moveLog;

    Utility::sendPacket(UserFacade::getUser( getPlayer(playerID)->getOpponentID() )->getConnectionID(), packet);

    for(auto& spectatorID : getMatch( getPlayer(playerID)->getMatchID() )->getSpectators() ){
        Utility::sendPacket(UserFacade::getUser(spectatorID)->getConnectionID(), packet);
    }

}

void GameControl::sendChatMessage(int playerID, string msg){
    int toPlayer = UserFacade::getUser(getPlayer(playerID)->getOpponentID())->getConnectionID();
    Utility::buildPacketHeader(playerID, packetID::Chat, packet);
    packet << msg;
    Utility::sendPacket(toPlayer, packet);

}


void GameControl::gameRequest(int playerID, int gameMode, int gameTime){
    switch(gameMode){
        case 0:
            classicRequest(playerID, gameTime);
        break;
        case 1:
            randomRequest(playerID, gameTime);
        break;
        case 2:
            capaRequest(playerID, gameTime);
        break;
        default:
            cout << "Something went very wrong..." << endl;
        break;
    }
}


void GameControl::classicRequest(int playerID, int gameTime){
    switch(gameTime){
        case 300:
            if(queueManager.classic5Queue.size() > 0){
                int enemy = queueManager.classic5Queue.front();

                queueManager.classic5Queue.pop();
                processRequest(playerID, enemy, gameType::Classic, gameTime, 1, 1);
            }
            else{
                queueManager.classic5Queue.push(playerID);
            }
        break;
        case 900:
            if(queueManager.classic15Queue.size() > 0){
                int enemy = queueManager.classic15Queue.front();
                queueManager.classic15Queue.pop();
                processRequest(playerID, enemy, gameType::Classic, gameTime, 1, 1);
            }
            else queueManager.classic15Queue.push(playerID);
        break;
        case 1800:
            if(queueManager.classic30Queue.size() > 0){
                int enemy = queueManager.classic30Queue.front();
                queueManager.classic30Queue.pop();
                processRequest(playerID, enemy, gameType::Classic, gameTime, 1, 1);
            }
            else queueManager.classic30Queue.push(playerID);
        break;
    }
}


void GameControl::randomRequest(int playerID, int gameTime){
    switch(gameTime){
        case 300:
            if(queueManager.random5Queue.size() > 0){
                int enemy = queueManager.random5Queue.front();
                queueManager.random5Queue.pop();
                processRequest(playerID, enemy, gameType::Fischer, gameTime, 1, 1);
            }
            else queueManager.random5Queue.push(playerID);
        break;
        case 900:
            if(queueManager.random15Queue.size() > 0){
                int enemy = queueManager.random15Queue.front();
                queueManager.random15Queue.pop();
                processRequest(playerID, enemy, gameType::Fischer, gameTime, 1, 1);
            }
            else queueManager.random15Queue.push(playerID);
        break;
        case 1800:
            if(queueManager.random30Queue.size() > 0){
                int enemy = queueManager.random30Queue.front();
                queueManager.random30Queue.pop();
                processRequest(playerID, enemy, gameType::Fischer, gameTime, 1, 1);
            }
            else queueManager.random30Queue.push(playerID);
        break;
    }
}


void GameControl::capaRequest(int playerID, int gameTime){
    switch(gameTime){
        case 300:
            if(queueManager.capa5Queue.size() > 0){
                int enemy = queueManager.capa5Queue.front();
                queueManager.capa5Queue.pop();
                processRequest(playerID, enemy, gameType::Capa, gameTime, 1, 1);
            }
            else queueManager.capa5Queue.push(playerID);
        break;
        case 900:
            if(queueManager.capa15Queue.size() > 0){
                int enemy = queueManager.capa15Queue.front();
                queueManager.capa15Queue.pop();
                processRequest(playerID, enemy, gameType::Capa, gameTime, 1, 1);
            }
            else queueManager.capa15Queue.push(playerID);
        break;
        case 1800:
            if(queueManager.capa30Queue.size() > 0){
                int enemy = queueManager.capa30Queue.front();
                queueManager.capa30Queue.pop();
                processRequest(playerID, enemy, gameType::Capa, gameTime, 1, 1);
            }
            else queueManager.capa30Queue.push(playerID);
        break;
    }
}


void GameControl::processRequest(int playerID, int enemy, gameType gameMode, int gameTime, bool isPublic, bool ranked){

    if(findPlayer(enemy) and UserFacade::getUser(enemy)->getStatus() == statusID::online){

        if(findPlayer(playerID) and UserFacade::getUser(playerID)->getStatus() == statusID::online){

            createMatch(getPlayer(playerID), getPlayer(enemy), static_cast<int>(gameMode), gameTime, isPublic, ranked);

            userFacade.setUserStatus(playerID, statusID::playing);
            userFacade.setUserStatus(enemy, statusID::playing);

        }

        sendGameOptions(enemy, UserFacade::getUser(playerID)->getConnectionID(), 0, static_cast<int>(gameMode), gameTime, ranked);
        sendGameOptions(playerID, UserFacade::getUser(enemy)->getConnectionID(), 1, static_cast<int>(gameMode), gameTime, ranked);

        Utility::buildPacketHeader(enemy, packetID::Name, packet);
        packet << getPlayer(enemy)->getNickname();
        Utility::sendPacket(UserFacade::getUser(playerID)->getConnectionID(), packet);

        Utility::buildPacketHeader(playerID, packetID::Name, packet);
        packet << getPlayer(playerID)->getNickname();
        Utility::sendPacket(UserFacade::getUser(enemy)->getConnectionID(), packet);


        if(ranked){
            Utility::buildPacketHeader(enemy, packetID::Elo, packet);
            packet << static_cast<int>(getPlayer(enemy)->getElo( static_cast<int>(gameMode), gameTime/900 ) );
            Utility::sendPacket(UserFacade::getUser(playerID)->getConnectionID(), packet);

            Utility::buildPacketHeader(playerID, packetID::Elo, packet);
            packet << static_cast<int>(getPlayer(playerID)->getElo( static_cast<int>(gameMode), gameTime/900 ) );
            Utility::sendPacket(UserFacade::getUser(enemy)->getConnectionID(), packet);
        }

        getPlayer(playerID)->setOpponentID(enemy);
        getPlayer(enemy)->setOpponentID(playerID);

    }
}


void GameControl::sendGameOptions(int playerID, int toPlayer, int playerColor, int gameMode, int gameTime, bool ranked){
    Utility::buildPacketHeader(playerID, packetID::GameOptions, packet);
    packet << playerColor << gameMode << gameTime << ranked;
    Utility::sendPacket(toPlayer, packet);
}

void GameControl::sendGiveUp(int playerID){
    Utility::buildPacketHeader(playerID, packetID::GiveUp, packet);
    Utility::sendPacket(UserFacade::getUser( getPlayer(playerID)->getOpponentID() )->getConnectionID(), packet );

    getMatch(getPlayer(playerID)->getMatchID())->~Match();
}


void GameControl::createMatch(Player* white, Player* black, int gameMode, int gameTime, int isPublic, int ranked){

    int matchID;
    if(ranked){
        matchID = numberOfMatches++;
    }
    else{
        matchID = Matches.size();
    }

    Match match(matchID, white, black, gameMode, gameTime, isPublic, ranked);
    white->setMatchID(match.getID());
    black->setMatchID(match.getID());

    Matches.push_back(match);

    if(match.getPublic()){
        twitterModule.sendTweet("Ongoing match: " + white->getNickname() + " X " + black->getNickname());
    }

}

void GameControl::endMatch(int matchID, int playerID, int condition){

    Match* match = getMatch(matchID);
    Player* winner;
    Player* loser;

    switch(condition){
        case 0: { //checkmate
            Utility::buildPacketHeader(playerID, packetID::Checkmate, packet);
            winner = getPlayer(playerID);
            loser = getPlayer(winner->getOpponentID());
        } break;

        case 1: { //giveup
            Utility::buildPacketHeader(playerID, packetID::GiveUp, packet);
            loser = getPlayer(playerID);
            winner = getPlayer(loser->getOpponentID());
        } break;

        case 2: { //timeout
            Utility::buildPacketHeader(playerID, packetID::GameEndTimeOut, packet);
            loser = getPlayer(playerID);
            winner = getPlayer(loser->getOpponentID());
        } break;

        case 3: { //disconnected
            Utility::buildPacketHeader(playerID, packetID::GameEnd, packet);
            loser = getPlayer(playerID);
            winner = getPlayer(loser->getOpponentID());
        } break;

        /*
        case 4: { //draw
            Utility::buildPacketHeader(winnerID, packetID::Draw, packet);
        } break;
        */
    }

    Utility::sendPacket(UserFacade::getUser(getPlayer(playerID)->getOpponentID())->getConnectionID(), packet);

    match->setWinner(winner);
    match->setLoser(loser);

    UserFacade::setUserStatus(winner->getID(), statusID::online);
    UserFacade::setUserStatus(winner->getOpponentID(), statusID::online);

    if(match->getRanked()){

        calculateELO(match, winner, loser);

        rankManager->updatePlayerRank(winner, match->getGameMode(), match->getGameTime()/900);
        rankManager->updatePlayerRank(loser, match->getGameMode(), match->getGameTime()/900);

        GameDAO::saveMatch(match);

        GameDAO::saveMatchInPGNFormat(match);

        GameDAO::savePlayers(Players);
    }

    GameDAO::logMatch(match);

    winner->setOpponentID(-1);
    loser->setOpponentID(-1);

    winner->setMatchID(-1);
    loser->setMatchID(-1);

    int i;
    for(i = 0; Matches[i].getID() != matchID; i++);
    Matches.erase(Matches.begin()+i);
    //delete match;
}

void GameControl::updateMatchBoardAndLog(int playerID, string newBoard[8][10], string moveLog){
    Match* match = getMatch(getPlayer(playerID)->getMatchID());
    if(match != nullptr){
        match->setBoard(newBoard);
        match->setHistory(match->getMatchHistory() + moveLog);
    }
}

void GameControl::removeFromQueue(int ID){
    queueManager.removeFromQueue(ID);
}

void GameControl::watchRequest(int playerID, int watchID){
    Match* match = getMatch(watchID);

    if(match->getPublic()){
        match->insertSpectator(playerID);
        sendWatchState(playerID, watchID);

        int numberOfSpectators = match->getSpectators().size();
        sendNumberWatching(playerID, numberOfSpectators);
        sendNumberWatching(match->getWhite()->getID(), numberOfSpectators);
        sendNumberWatching(match->getBlack()->getID(), numberOfSpectators);
    }
}


Player* GameControl::getPlayer(string nickname){
    for(auto& player : Players){
        if(player.getNickname() == nickname)
            return &player;
    }
    return nullptr;
}

Player* GameControl::getPlayer(int ID){
    for(auto& player : Players){
        if(player.getID() == ID) return &player;
    }
    return nullptr;
}

Match* GameControl::getMatch(int ID){
    for(auto& match : Matches){
        if(match.getID() == ID) return &match;
    }
    return nullptr;
}

int GameControl::getNumberOfMatches(){
    return numberOfMatches;
}

bool GameControl::findPlayer(int ID){
    if(getPlayer(ID) == nullptr){
        return false;
    }
    else{
        return true;
    }

}
