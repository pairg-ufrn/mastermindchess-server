#ifndef _USERDAO
#define _USERDAO
#include "user.h"
#include "configurationDAO.h"

class UserDAO{
    public:
        static vector<User> loadAccounts();
        static bool saveAccounts(vector<User> userlist);
};

#endif // _USERDAO
