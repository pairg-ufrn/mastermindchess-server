#pragma once

#include "player.h"
#include <iostream>

class Player;

class RankManager{
    public:
        void loadRanks(vector<Player>& players);
        void sortRank(int gameMode, int gameTime);
        void rankPlayer(Player* player, int gameMode, int gameTime);
        void updatePlayerRank(Player* player, int gameMode, int gameTime);
        void addNewPlayerToRank(Player* player);
        void updatePlayersReference(vector<Player>& players);
        int getPlayerRank(Player* player, int gameMode, int gameTime);
        vector<Player*> getRank(int gameMode, int gameTime);

       RankManager();

    private:
        vector <Player*> playersReference;
        vector < vector < Player* > > classicRank;
        vector < vector < Player* > > capaRank;
        vector < vector < Player* > > fischerRank;
        vector< vector < vector < Player* > > > Rank;
};

