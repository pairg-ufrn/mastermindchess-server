#ifndef _QUEUEMANAGER
#define _QUEUEMANAGER
#include <queue>
#ifndef queueManager
#define queueManager QueueManager::getInstance()
#endif

class QueueManager{

    public:
        static QueueManager& getInstance();
        std::queue<int>        classic5Queue;
        std::queue<int>        capa5Queue;
        std::queue<int>        random5Queue;

        std::queue<int>        classic15Queue;
        std::queue<int>        capa15Queue;
        std::queue<int>        random15Queue;

        std::queue<int>        classic30Queue;
        std::queue<int>        capa30Queue;
        std::queue<int>        random30Queue;

        void removeFromQueue(int playerID);

     private:
        static QueueManager* instance;
        QueueManager(){};
        QueueManager(QueueManager const&);
        void operator=(QueueManager const&);

};

#endif // _QUEUEMANAGER
