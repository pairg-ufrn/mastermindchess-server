#include "queueManager.h"

QueueManager& QueueManager::getInstance(){
    static QueueManager instance;
    return instance;
}

void QueueManager::removeFromQueue(int playerID){
    if(classic5Queue.front() == playerID){
        classic5Queue.pop();
    }
    if(classic15Queue.front() == playerID){
        classic15Queue.pop();
    }
    if(classic30Queue.front() == playerID){
        classic30Queue.pop();
    }

    if(random5Queue.front() == playerID){
        random5Queue.pop();
    }
    if(random15Queue.front() == playerID){
        random15Queue.pop();
    }
    if(random30Queue.front() == playerID){
        random30Queue.pop();
    }

    if(capa5Queue.front() == playerID){
        capa5Queue.pop();
    }
    if(capa15Queue.front() == playerID){
        capa15Queue.pop();
    }
    if(capa30Queue.front() == playerID){
        capa30Queue.pop();
    }
}
