#include <iostream>
#include <math.h>
#include <memory>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <map>
#include <chrono>
#include <ctime>
#include "utility.h"

using namespace std;


void Utility::buildPacketHeader(int fromPlayer, packetID pktID, sf::Packet& packet){
    packet.clear();
    packet << fromPlayer << static_cast<int>(pktID);
}


void Utility::sendPacket(int toID, sf::Packet& packet){
    connectionHandler.lastSendedPacket = packet;
    //connectionHandler.lastSendedPacketReceiver = UserFacade::getUserByConnection(toID)->getID();
    connectionHandler.connections[toID]->send(packet);
    ServerDAO::logPacket(packet, "out", toID);
}


bool Utility::isEmpty(std::ifstream& pFile){
    return pFile.peek() == std::ifstream::traits_type::eof();
}

int Utility::StringToNumber ( const string &Text ){  //Text not by const reference so that the function can be used with a
	stringstream ss(Text);  //character array as argument
	int result;
	return ss >> result ? result : 0;
}


string Utility::getDatestamp(string separator){
	using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output << now_in.tm_year + 1900 << separator << padIt(tm_mon + 1) << separator << padIt(tm_mday);
    return output.str();
}

string Utility::toString(const double& number){
    std::ostringstream ss;
    ss << number;
    return ss.str();
}

string Utility::getTimestamp () {
    using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output <<  padIt(tm_hour) << ":" << padIt(tm_min) << ":" << padIt(tm_sec)
           << "";
    return output.str();
}

string Utility::getTimestamp(string separator){
	using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output <<  padIt(tm_hour) << separator << padIt(tm_min) << separator << padIt(tm_sec)
           << "";
    return output.str();
}


string Utility::getDatestamp (bool date, bool hour) {
    using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output << "";
        if(date){
            output  <<  padIt(tm_mday)    << "/"
                    <<  padIt(tm_mon + 1) << "/"
                    <<  now_in.tm_year + 1900 << " ";
        }
        if(hour){
            output  <<  padIt(tm_hour) << ":" << padIt(tm_min) << ":" << padIt(tm_sec)
                    << "";
        }
    return output.str();
}
