#ifndef _CONNOTFNDEXP
#define _CONNOTFNDEXP
#include "exceptions.h"

class ConnectionNotFoundException : runtime_error {
    public:

        ConnectionNotFoundException(int _CID) : runtime_error( "Connection not found" ), cid( _CID ){}

        virtual const char* what() const throw() {
            return runtime_error::what();
        }


        int getCID() const {
            return cid;
        }

    private:
        int cid;
};

#endif // _CONNOTFNDEXP

