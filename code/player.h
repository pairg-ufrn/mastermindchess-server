#pragma once

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <vector>
#include "dataType.h"
#include "userFacade.h"

using namespace std;

class Player{
    public:
        std::string getNickname();

        int getID();
        int getK();

        int getElo(int gameMode, int gameTime);
        int getVictories(int gameMode, int gameTime);
        int getDefeats(int gameMode, int gameTime);
        int getDraws(int gameMode, int gameTime);

        int getOpponentID();
        int getMatchID();

        statusID getStatus();

        void setID(int ID);
        void setNickname(std::string nickname);
        void setStatus(statusID status);
        void setElo(int elo, int gameMode, int gameTime);
        void setVictories(int num, int gameMode, int gameTime);
        void setDefeats(int num, int gameMode, int gameTime);
        void setDraws(int num, int gameMode, int gameTime);

        void setOpponentID(int ID);
        void setMatchID(int ID);

        void raiseVictories(int gameMode, int gameTime);
        void raiseDefeats(int gameMode, int gameTime);
        void raiseDraws(int gameMode, int gameTime);

        Player();
        Player(int ID);

    private:

        statusID status;

        int ID;

        int K;
        int number_of_matches;
        int elo[3][3];
        int victories[3][3];
        int defeats[3][3];
        int draws[3][3];

        int opponentID;
        int matchID;
};

