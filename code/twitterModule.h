#ifndef _TWITTER
#define _TWITTER
#include <SFML/Network.hpp>

#include <iostream>

#include "configurationDAO.h"

#ifndef twitterModule
#define twitterModule TwitterModule::getInstance()
#endif

class TwitterModule{

    public:
        static TwitterModule& getInstance();
        void connect();
        void sendTweet(std::string message);

        TwitterModule(){
            port = 20315;
            address = "127.0.0.1";
        }

    private:
        int port;
        std::string address;
        sf::UdpSocket socket;

};

#endif // _TWITTER
