#include "userControl.h"
#include "exceptions.h"

UserControl& UserControl::getInstance(){
    static UserControl instance;
    return instance;
}

UserControl::UserControl(){
    Users = UserDAO::loadAccounts();
}


User* UserControl::getUser(int ID){
    User* result = nullptr;

    for(auto& user : Users){
        if(user.getID() == ID){
            result = &user;
        }
    }

    if(result != nullptr){
        return result;
    }
    else{
        ServerDAO::logCrash("User " + Utility::toString(ID) + " not found");
        throw UserNotFoundException(ID);
    }

}

User* UserControl::getUserByConnection(int connectionID){

    User* result = nullptr;

    for(auto& user : Users){
        if(user.getConnectionID() == connectionID){
            result = &user;
        }
    }

    if(result != nullptr){
        return result;
    }
    else{
        ServerDAO::logCrash("Connection not found");
        throw ConnectionNotFoundException(connectionID);
    }
}

vector<User> UserControl::getUsers(){
    return this->Users;
}


User* UserControl::getUser(std::string name){
    User* result = nullptr;

    for(auto& user : Users){
        if(user.getUsername() == name or user.getNickname() == name or user.getEmail() == name){
            result = &user;
        }
    }

    if(result != nullptr){
        return result;
    }
    else{
        ServerDAO::logCrash("User " + name + " not found");
        throw UserNotFoundException(name);;
    }


}

bool UserControl::findUser(std::string name){
    for(auto& user : Users){
        if(user.getUsername() == name or user.getNickname() == name or user.getEmail() == name){
            return true;
        }
    }
    return false;
}

bool UserControl::findUser(int ID){
    for(auto& user : Users){
        if(user.getID() == ID){
            return true;
        }
    }
    return false;
}


bool UserControl::createUser(std::string uusername, std::string upassword, std::string unickname, std::string uemail, bool twttrUpdt){
    if(not findUser(uusername) and not findUser(unickname) and not findUser(uemail)){
        User user(Users.size(), uusername, unickname, upassword, uemail, Utility::getDatestamp(true, true), twttrUpdt);
        Users.push_back(user);
        UserDAO::saveAccounts(Users);
        return true;
    }
    else return false;
}
