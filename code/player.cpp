#include "player.h"


int Player::getID(){
    return ID;
}

std::string Player::getNickname(){
    return UserFacade::getUser(getID())->getNickname();
}

int Player::getElo(int gameMode, int gameTime){
    return elo[gameMode][gameTime];
}


int Player::getVictories(int gameMode, int gameTime){
    return victories[gameMode][gameTime];
}


int Player::getDefeats(int gameMode, int gameTime){
    return defeats[gameMode][gameTime];
}



int Player::getDraws(int gameMode, int gameTime){
    return draws[gameMode][gameTime];
}


statusID Player::getStatus(){
    return status;
}

int Player::getOpponentID(){
    return opponentID;
}

int Player::getMatchID(){
    return matchID;
}

void Player::setID(int ID){
    this->ID = ID;
}

void Player::setVictories(int num, int gameMode, int gameTime){
    this->victories[gameMode][gameTime] = num;
}

void Player::setDefeats(int num, int gameMode, int gameTime){
    this->defeats[gameMode][gameTime] = num;
}

void Player::setDraws(int num, int gameMode, int gameTime){
    this->draws[gameMode][gameTime] = num;
}


void Player::setElo(int elo, int gameMode, int gameTime){
    this->elo[gameMode][gameTime] = elo;
}

void Player::setOpponentID(int ID){
    this->opponentID = ID;
}

void Player::setMatchID(int ID){
    this->matchID = ID;
}


void Player::raiseVictories(int gameMode, int gameTime){
    this->victories[gameMode][gameTime]++;
}


void Player::raiseDefeats(int gameMode, int gameTime){
    this->defeats[gameMode][gameTime]++;
}


void Player::raiseDraws(int gameMode, int gameTime){
    this->draws[gameMode][gameTime]++;
}


 Player::Player(int _ID){
    ID = _ID;

    K = 30;
    number_of_matches = 0;

    int i, j;
    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
            elo[i][j] = 950;
            victories[i][j] = 0;
            defeats[i][j] = 0;
            draws[i][j] = 0;
        }
    }


}


 Player::Player(){
    ID = -1;

    K = 30;

    number_of_matches = 0;

    int i, j;
    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
            elo[i][j] = 950;
            victories[i][j] = 0;
            defeats[i][j] = 0;
            draws[i][j] = 0;
        }
    }



}
