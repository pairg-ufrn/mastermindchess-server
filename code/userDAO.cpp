#include "userDAO.h"

vector<User> UserDAO::loadAccounts(){
    ifstream input {configurationDAO.getDataDirectory() + "users"};
    vector<User> userlist;
    string id, tempUser, tempNick, tempPass, tempEmail, tempRegData, tempActive, tempTwttr;

    int i = 0;

    if(!Utility::isEmpty(input)){

        while(getline(input, id, ';')){

            if(id != "\n" and id != ""){

                userlist.emplace_back();

                getline(input, tempUser,    ';');
                getline(input, tempNick,    ';');
                getline(input, tempPass,    ';');
                getline(input, tempEmail,   ';');
                getline(input, tempRegData, ';');
                getline(input, tempActive,  ';');
                getline(input, tempTwttr,   ';');

                userlist[i].setID           (Utility::StringToNumber(id));
                userlist[i].setUsername     (tempUser);
                userlist[i].setNickname     (tempNick);
                userlist[i].setPassword     (tempPass);
                userlist[i].setEmail        (tempEmail);
                userlist[i].setRegisterData (tempRegData);
                userlist[i].setActive       (tempActive);
                userlist[i].setTwitter      (Utility::StringToNumber(tempTwttr));
            }

            i++;
            if(input.peek() == EOF or input.peek() == 0) break;
        }
    }
    return userlist;
}


bool UserDAO::saveAccounts(vector<User> userlist){
    ofstream outputData {configurationDAO.getDataDirectory() + "users"};

    for (auto& user : userlist){
        outputData  << user.getID()           << ";"
                    << user.getUsername()     << ";"
                    << user.getNickname()     << ";"
                    << user.getPassword()     << ";"
                    << user.getEmail()        << ";"
                    << user.getRegisterData() << ";"
                    << user.getActive()       << ";"
                    << user.getTwitter()      << ";\n";
    }
    outputData << std::flush;
}
