#include "twitterModule.h"


TwitterModule& TwitterModule::getInstance(){
    static TwitterModule instance;
    return instance;
}


void TwitterModule::connect(){
/*
    if (socket.connect(address, port) == sf::Socket::Done) {
        socket.setBlocking(false);
        std::cout << "Twitter Module connected!" << std::endl;
    }
    else{
        std::cout << "Could not connect to Twitter Module..." << std::endl;
    }
*/
}

void TwitterModule::sendTweet(std::string message){
    if(configurationDAO.getTwitterModule()){
        const void* data = message.c_str();
        std::size_t sizeOfData = message.length();
        socket.send(data, sizeOfData, address, port);
    }

}
