#include "userFacade.h"

UserFacade& UserFacade::getInstance(){
    static UserFacade instance;
    return instance;
}


User* UserFacade::getUser(int ID){
    return userControl.getUser(ID);
}

User* UserFacade::getUserByConnection(int ID){
    return userControl.getUserByConnection(ID);
}

User* UserFacade::getUser(std::string username){
    return userControl.getUser(username);
}

bool UserFacade::findUser(std::string username){
    return userControl.findUser(username);
}

int UserFacade::getNumberOfUsers(){
    return userControl.getUsers().size();
}

vector<User> UserFacade::getAllUsers(){
    return userControl.getUsers();
}

void UserFacade::setUserConnectionID(int userID, int connectionID){
    userControl.getUser(userID)->setConnectionID(connectionID);
}

bool UserFacade::setUserStatus(int ID, statusID status){
    if(userControl.findUser(ID)){
        userControl.getUser(ID)->setStatus(status);
        return true;
    }
    return false;
}

int UserFacade::handleRequest(sf::Packet packet, int pktID, int fromUser){
    switch(static_cast<packetID>(pktID)){
        case packetID::Register : {
            string uusername, upassword, unickname, uemail;
            bool twitter;
            packet >> uusername >> upassword >> unickname >> uemail >> twitter;

            Utility::buildPacketHeader(fromUser, packetID::RegisterResponse, packet);

            if(userControl.createUser(uusername, upassword, unickname, uemail, twitter)){
                ServerDAO::logHistory ("new account", unickname, fromUser);
                GameFacade::registerPlayer(userControl.getUser(uusername)->getID());
                packet << true;
            }
            else{
                packet << false;
            }

            Utility::sendPacket(fromUser, packet);
        }
        break;

    }
}




