#include "serverFacade.h"

void ServerFacade::handleRequest(sf::Packet packet, int pktID, int connectionID){

    switch(static_cast<packetID>(pktID)){
        case packetID::Login : {
            string ulogin, upassword, version;
            packet >> ulogin >> upassword >> version;
            if(ulogin.size() > 0 && upassword.size() > 0){
                ServerControl::processLogin(connectionID, ulogin, upassword, version);
            }
        } break;


        case packetID::Disconnect: {
            ServerControl::disconnectUser(UserFacade::getUserByConnection(connectionID)->getID());
        } break;

        case packetID::KeepAlive:
            try{
                ServerControl::keepUserAlive(connectionID);
            }
            catch(exception const& exc){
                cout << "connectionID " << connectionID << "came from an unexisting user. Erro." << endl;
            }
        break;

    }

}
