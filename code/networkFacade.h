#ifndef _NETFACADE
#define _NETFACADE
#include <SFML/Network.hpp>

#include "dataType.h"
#include "gameFacade.h"
#include "serverFacade.h"


class NetworkFacade{

    public:
        static void handleRequest(sf::Packet packet, int userID);
};

#endif // _NETFACADE
