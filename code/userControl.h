#pragma once

#include <vector>
#include "userDAO.h"

#ifndef userControl
#define userControl UserControl::getInstance()
#endif

class UserControl{
    private:
        static UserControl* instance;
        UserControl();
        UserControl(UserControl const&);
        void operator=(UserControl const&);
        vector<User> Users;

    public:
        static UserControl& getInstance();
        User* getUser(int ID);
        User* getUser(std::string name);
        User* getUserByConnection(int connectionID);
        vector<User> getUsers();

        bool findUser(std::string name);
        bool findUser(int ID);


        bool createUser(std::string uusername, std::string upassword, std::string unickname, std::string uemail, bool twitter);

        bool deleteUser(int ID);

};
