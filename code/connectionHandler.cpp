#include "connectionHandler.h"
#include "configurationDAO.h"
#include "twitterModule.h"
#include "exceptions.h"

sf::Clock   sendListTime;
sf::Clock   updateTime;


ConnectionHandler& ConnectionHandler::getInstance(){
    static ConnectionHandler instance;
    return instance;
}

void ConnectionHandler::run(){
    lastLogin = -1;
    lastLogout = -1;
    listener.setBlocking(false);
    while (listener.listen (configurationDAO.getPort()) != sf::Socket::Done);
    connections.emplace_back (new sf::TcpSocket());

    cout << "---------- MasterMind Chess Server v0.1.14 ---------- \n\n";
    cout << "\nSuccessfully binded to port " << configurationDAO.getPort() << endl;
    cout << "Server started!"                                   << endl;

    ServerDAO::logHistory ("System initialized at port " + Utility::toString(configurationDAO.getPort()), sf::IpAddress::getPublicAddress().toString());

    while (true) {
        if(updateTime.getElapsedTime().asSeconds() >= 8){
            updateTime.restart();
            ServerControl::sendMatchesList();
            ServerControl::disconnectOfflineUsers();
        }
        acceptNewConnections();
        receivePacket();

        /* SEND TO SERVER LAYER*/
        if(sendListTime.getElapsedTime().asSeconds() >= 4){
            ServerControl::sendUsersList();
            sendListTime.restart();
        }

        sf::sleep(sf::milliseconds(400));
    }
}


void ConnectionHandler::receivePacket(){
    int connectionID = 0;
    for (auto& connection : connections){
        if (connection->receive(packet) == sf::Socket::Done) {
            lastReceivedPacket = packet;
            lastReceivedPacketSender = connectionID;
            ServerDAO::logPacket(packet, "in", connectionID);
            NetworkFacade::handleRequest(packet, connectionID);
        }
        connectionID++;
    }
}

int ConnectionHandler::getLastReceivedPacketID(){
    sf::Packet packet = lastReceivedPacket;
    int aux, pktID;
    packet >> aux >> pktID;
    return pktID;
}

int ConnectionHandler::getLastSendedPacketID(){
    sf::Packet packet = lastSendedPacket;
    int aux, pktID;
    packet >> aux >> pktID;
    return pktID;
}

int ConnectionHandler::getOnlineUsers(){
    return ServerControl::getNumberOfOnlineUsers();
}


void ConnectionHandler::acceptNewConnections(){
    if (listener.accept(*connections.back()) == sf::Socket::Done) {
        connections.back()->setBlocking (false);
        connections.emplace_back (new sf::TcpSocket());
    }
}
