#pragma once

#include "player.h"
#include "utility.h"

class Player;

class Match{
    private:
        Player* white;
        Player* black;
        vector<int> spectators;
        string matchHistory;
        string board[8][10];

        int ID;
        int gameTime, gameMode;
        Player* winner;
        Player* loser;
        int eloWon, eloLost;
        int status;
        int numberWatching;
        int isPublic;
        int ranked;

    public:
        sf::Clock whiteClock, blackClock;
        double whiteTime, blackTime;
        string startTime, endTime;

        Player* getWhite();
        Player* getBlack();
        Player* getWinner();
        Player* getLoser();

        int getGameMode();
        int getGameTime();
        int getID();
        int getEloWon();
        int getEloLost();
        int getPublic();
        int getRanked();
        int getStatus();

        string getMatchHistory();
        string getBoard(int i, int j);

        vector<int> getSpectators();

        void insertSpectator(int playerID);

        void setWinner(Player* winner);
        void setLoser(Player* loser);
        void setEloWon(int eloWon);
        void setEloLost(int eloLost);
        void setBoard(string newBoard[8][10]);
        void setHistory(string history);


        Match(){};
        Match(int ID, Player* white, Player* black, int gameMode, int gameTime, int isPublic, int ranked);
        ~Match();


};

