#include "gameFacade.h"

void GameFacade::handleRequest(sf::Packet packet, int pktID, int connectionID){
    User* user = UserFacade::getUserByConnection(connectionID);
    int playerID;
    if(user != nullptr){
         playerID = user->getID();
    }
    else{
        throw "User not found!";
    }


    if(UserFacade::getUser(playerID)->getStatus() != statusID::offline){

        switch(static_cast<packetID> (pktID) ){

            case packetID::BoardStatus: {
                int k, p;
                string newBoard[8][10];
                for(k = 0; k < 8; k++){
                    for(p = 0; p < 10; p++){
                        packet >> newBoard[k][p];
                    }
                }
                gameControl.updateMatchBoardAndLog(playerID, newBoard, "");
            }
            break;

            case packetID::GameRequest: {
                int gameTime, gameMode;
                packet >> gameMode >> gameTime;

                gameControl.gameRequest(playerID, gameMode, gameTime);
                break;
            }

            case packetID::GameInvite: {
                string name;
                int gameMode, gameTime;
                bool isPublic;
                packet >> name >> gameMode >> gameTime >> isPublic;
                gameControl.sendInviteRequest(playerID, name, gameMode, gameTime, isPublic);
                break;
            }

            case packetID::InviteResponse: {
                string name;
                bool response;
                int gameMode, gameTime;
                bool isPublic;
                packet >> name >> response;
                if(response){
                    packet >> gameMode >> gameTime >> isPublic;
                }

                gameControl.sendInviteResponse(playerID, name, gameMode, gameTime, isPublic, response);

                break;
            }

            case packetID::Move: {
                int i, j, iP, jP, k, p;
                bool check;
                string moveLog;
                string newBoard[8][10];
                packet >> i >> j >> iP >> jP >> check >> moveLog;

                for(k = 0; k < 8; k++){
                    for(p = 0; p < 10; p++){
                        packet >> newBoard[k][p];
                    }
                }
                gameControl.updateMatchBoardAndLog(playerID, newBoard, moveLog);

                gameControl.sendMove(playerID, i, j, iP, jP, check, moveLog);
                break;
            }

            case packetID::FischerPieceOrder:{
                int k, p;
                string newBoard[8][10];
                for(k = 0; k < 8; k++){
                    for(p = 0; p < 10; p++){
                        packet >> newBoard[k][p];
                    }
                }
                gameControl.sendFischerPiecesOrder(playerID, newBoard);
            } break;


            case packetID::Chat : {
                string msg;
                packet >> msg;
                gameControl.sendChatMessage(playerID, msg);
            }

            case packetID::ExitQueue:
                gameControl.removeFromQueue(playerID);
            break;


            case packetID::Checkmate: {
                gameControl.endMatch(getPlayer(playerID)->getMatchID(), playerID, 0);
                break;
            }

            case packetID::GiveUp: {
                gameControl.endMatch(getPlayer(playerID)->getMatchID(), playerID, 1);
                break;
            }

            case packetID::GameEndTimeOut : {
                gameControl.endMatch(getPlayer(playerID)->getMatchID(), playerID, 2);
            }

            case packetID::WatchRequest: {
                int watchID;
                packet >> watchID;
                gameControl.watchRequest(playerID, watchID);
                break;
            }

        }
    }


}

int GameFacade::getNumberOfPlayers(){
    return gameControl.getPlayers().size();
}

int GameFacade::getNumberOfMatches(){
    return gameControl.getMatches().size();
}

vector<Match> GameFacade::getMatches(){
    return gameControl.getMatches();
}

void GameFacade::registerPlayer(int ID){
    gameControl.registerPlayer(ID);
}

Player* GameFacade::getPlayer(int ID){
    return gameControl.getPlayer(ID);
}
