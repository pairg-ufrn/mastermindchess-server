#include "user.h"



int User::getID(){
    return this->ID;
}


std::string User::getUsername(){
    return this->username;
}


std::string User::getNickname(){
    return this->nickname;
}


std::string User::getPassword(){
    return this->password;
}


std::string User::getEmail(){
    return this->email;
}


std::string User::getRegisterData(){
    return this->registerdata;
}


std::string User::getActive(){
    return this->active;
}

statusID User::getStatus(){
    return this->status;
}

int User::getConnectionID(){
    return this->connectionID;
}

bool User::getTwitter(){
    return this->twitter;
}

void User::setID(int ID){
    this->ID = ID;
}

void User::setUsername(std::string username){
    this->username = username;
}

void User::setNickname(std::string nickname){
    this->nickname = nickname;
}


void User::setPassword(std::string password){
    this->password = password;
}


void User::setEmail(std::string email){
    this->email = email;
}


void User::setRegisterData(std::string registerdata){
    this->registerdata = registerdata;
}

void User::setActive(std::string active){
    this->active = active;
}

void User::setTwitter(bool twt){
    this->twitter = twt;
}

void User::setStatus(statusID status){
    this->status = status;
}


void User::setConnectionID(int id){
    this->connectionID = id;
}

void User::keepAlive(){
    this->connectionClock.restart();
}

bool User::isAlive(){
    bool alive = false;
    if(connectionClock.getElapsedTime().asSeconds() <= 20){
        alive = true;
    }
    return alive;
}

User::User(){
    this->status = statusID::offline;
    connectionID = -1;
}


User::User(int _ID, string _username, string _nickname, string _password, string _email, string _registerdata, bool twt){
    ID           = _ID;
    username     = _username;
    nickname     = _nickname;
    password     = _password;
    email        = _email;
    registerdata = _registerdata;
    active       = "1";
    status       = statusID::offline;
    twitter      = twt;
    connectionID = -1;
}
