#ifndef _EXCEPTIONS
#define _EXCEPTIONS

#include <iostream>
#include <exception>
#include <stdexcept>
#include <sstream>

using namespace std;

#include "connectionNotFoundException.h"

#include "userNotFoundException.h"


#endif // _EXCEPTIONS

