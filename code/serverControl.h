#ifndef _SERVERCONTROL
#define _SERVERCONTROL

#define SEND_USERSLIST_DELAY_TIME 5
#define SEND_PLAYERSLIST_DELAY_TIME 16
#define SEND_MATCHESLIST_DELAY_TIME 12

#include "connectionHandler.h"
#include "userControl.h"
#include "utility.h"
#include "serverDAO.h"
#include "match.h"

class ServerControl{
    public:
        static void initialize();

        void broadcast   (std::string message);

        static bool processLogin(int userID, std::string username, std::string password, std::string version);

        static int getNumberOfOnlineUsers();

        static bool connectUser(int ID);
        static void sendUsersList();
        static void keepUserAlive(int connectionID);
        static void sendPlayersList(int gameMode, int gameTime);
        static void sendMatchesList();

        static bool disconnectUser(int ID);
        static void disconnectOfflineUsers();

    private:
        static int numberOfOnlineUsers;



};

#endif // _SERVERCONTROL
