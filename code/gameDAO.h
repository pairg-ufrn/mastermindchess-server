#ifndef _GAMEDAO
#define _GAMEDAO
#include "match.h"
#include "configurationDAO.h"

class Match;
class Player;
class GameDAO{
    public:
        static vector<Player> loadPlayers();
        static void savePlayers(vector<Player>);

        static int loadMatches();
        static void logMatch(Match* match);
        static void saveMatch(Match* match);
        static void saveMatchInPGNFormat(Match* match);



};

#endif // _GAMEDAO
