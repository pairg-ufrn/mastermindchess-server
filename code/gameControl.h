#pragma once

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include "match.h"
#include "serverControl.h"
#include "queueManager.h"
#include "userFacade.h"
#include "gameDAO.h"
#include "rankManager.h"
#include "twitterModule.h"

#ifndef gameControl
#define gameControl GameControl::getInstance()
#endif

sf::Packet& operator<<(sf::Packet& packet, std::vector<std::vector<std::basic_string<char>>> vet);

class Match;

class RankManager;

class GameControl{

    public:
        static GameControl& getInstance();
        sf::Packet packet;

        bool findPlayer(int ID);
        Player* getPlayer(string nickname);
        Player* getPlayer(int ID);
        vector<Player> getPlayers();
        vector<Match> getMatches();
        Match* getMatch(int ID);

        void calculateELO           (Match* match, Player* winner, Player* loser);
        int getELOScore             (int Ra, int Rb, double Sa);
        int getNumberOfMatches      ();
        void watchRequest           (int playerID, int watchID);
        void gameRequest            (int playerID, int gameMode, int gameTime);
        void processRequest         (int playerID, int enemyID, gameType gameMode, int gameTime, bool isPublic, bool ranked);
        void sendInviteRequest      (int playerID, string name, int gameMode, int gameTime, bool isPublic);
        void sendMove               (int playerID, int i, int j, int iP, int jP, bool check, string moveLog);
        void sendMoveLog            (int playerID, int toPlayer, string moveLog);
        void sendGameEnd            (int playerID, int toPlayer);
        void sendGiveUp             (int playerID);
        void sendCheckMate          (int playerID, int toPlayer);
        void sendChatMessage        (int playerID,  string msg);
        void sendSpectatorChatMessage(int playerID, string msg);
        void sendInviteResponse     (int playerID, string invitee, int gameMode, int gameTime, bool isPublic, bool response);
        void sendInviteRejection    (int playerID, int toPlayer, string user);
        void sendWatchState         (int playerID, int matchID);
        void sendNumberWatching     (int playerID, int number);
        void sendFischerPiecesOrder (int playerID, string newBoard[][10]);
        void sendGameOptions        (int playerID, int toPlayer, int gameColor, int gameMode, int gameTime, bool ranked);

        void updateMatchBoardAndLog (int playerID, string newBoard[8][10], string moveLog);

        void createMatch            (Player* white, Player* black, int gameMode, int gameTime, int isPublic, int ranked);
        void endMatch               (int matchID, int winnerID, int condition);
        void registerPlayer         (int ID);
        void removeFromQueue        (int ID);

        vector<Player*> getRank   (int gameMode, int gameTime);



    private:
        static GameControl* instance;
        GameControl();
        GameControl(GameControl const&);
        void operator=(GameControl const&);

        vector<Match> Matches;
        vector<Player> Players;
        RankManager* rankManager;
        int numberOfMatches;

        void classicRequest (int playerID, int gameTime);
        void capaRequest    (int playerID, int gameTime);
        void randomRequest  (int playerID, int gameTime);
};

