#ifndef _USERFACADE
#define _USERFACADE
#include "user.h"
#include "userControl.h"
#include "serverControl.h"

#ifndef userFacade
#define userFacade UserFacade::getInstance()
#endif

class UserFacade{
    public:
        static UserFacade& getInstance();
        static int handleRequest(sf::Packet packet, int pktID, int fromUser);
        static User* getUser(int ID);
        static User* getUser(std::string username);
        static User* getUserByConnection(int ID);
        static bool findUser(std::string username);
        static bool setUserStatus(int ID, statusID status);
        static void setUserConnectionID(int userID, int connectionID);
        static int getNumberOfUsers();
        static vector<User> getAllUsers();

    private:
        static UserFacade* instance;
        UserFacade(){};
        UserFacade(UserFacade const&);
        void operator=(UserFacade const&);

};

#endif // _USERFACADE

