#include "networkFacade.h"

void NetworkFacade::handleRequest(sf::Packet packet, int connectionID) {
    int i, pktID, toPlayer;

    string movelog;
    string msg;
    packet >> toPlayer >> pktID ;

    switch (static_cast<packetID> (pktID)) {

        case packetID::Login :
            ServerFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::Register :
            UserFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::Move:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::BoardStatus:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::FischerPieceOrder: {
            GameFacade::handleRequest(packet, pktID, connectionID);
        } break;

        case packetID::Chat: {
            GameFacade::handleRequest(packet, pktID, connectionID);
        } break;


        case packetID::SpectatorChat:
            GameFacade::handleRequest(packet, pktID, connectionID);
            packet >> msg;
          //  network.sendSpectatorChatMessage(playerID, msg);
        break;

        case packetID::MatchHistory:
            GameFacade::handleRequest(packet, pktID, connectionID);
            packet >> msg;
           // network.saveMatchHistory(playerID, msg);
        break;

        case packetID::Checkmate: {
            GameFacade::handleRequest(packet, pktID, connectionID);
           // network.sendCheckMate(playerID, toPlayer);
            break;
        }

        case packetID::GiveUp:{
            GameFacade::handleRequest(packet, pktID, connectionID);
            break;
        }

        case packetID::GameEnd :{
            GameFacade::handleRequest(packet, pktID, connectionID);

           // network.sendGameEnd(playerID, toPlayer);
            break;
        }
        case packetID::GameEndTimeOut :
            GameFacade::handleRequest(packet, pktID, connectionID);
            {
            //cout << "GameEndTimeout from player " << playerID << endl;
           // network.sendGameEnd(playerID, toPlayer);
        break;
        }

        case packetID::PlayerList:
            ServerFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::Disconnect :
            ServerFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::GameRequest:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::GameInvite:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::InviteResponse:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::WatchRequest:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::ExitQueue:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        case packetID::MatchLog :
        //    string log;
        //    string player1, player2;
        //    packet >> log;
        //    playersEnemy[fromPlayer];
        break;

        case packetID::KeepAlive:
            ServerFacade::handleRequest(packet, pktID, connectionID);
        break;
        default : { //redirect
            //if (toPlayer < 0 || toPlayer > network.players.size() -1) return;
            //network.players[toPlayer]->send(packet);

            return; //do not update player list
        }

    }
    //Since a new player might have entered, or 2 of them are now playing,
    //it's necessary to send a packet updating the Playerlist;
//    if (playerID >= 0)
        //network.playersTime[playerID].restart();

}
