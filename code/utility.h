#ifndef _UTIL
#define _UTIL

#include "dataType.h"
#include "connectionHandler.h"
#include <SFML/Network.hpp>
#include <SFML/System.hpp>


using namespace std;

class Utility{
    public:
        static void buildPacketHeader(int fromPlayer, packetID pktID, sf::Packet& packet);

        static void sendPacket  (int toID, sf::Packet& packet);

        static bool isEmpty(std::ifstream& pFile);

        static int StringToNumber ( const string &Text );

        static string getDatestamp (string separator);

        static string getTimestamp ();

        static string getTimestamp(string separator);

        static string toString(const double& strg);

        static string getDatestamp (bool date, bool hour);

        static void loadConfiguration();

        static void parse(std::ifstream& file);
};


#endif // _UTIL
