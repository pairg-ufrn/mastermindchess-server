#ifndef _USER
#define _USER
#include "dataType.h"


class User{
    public:
        int getID();
        int getConnectionID();
        statusID getStatus();
        bool isAlive();
        std::string getUsername();
        std::string getNickname();
        std::string getPassword();
        std::string getEmail();
        std::string getRegisterData();
        std::string getActive();
        bool getTwitter();

        void setID(int ID);
        void setUsername(std::string username);
        void setNickname(std::string nickname);
        void setPassword(std::string password);
        void setEmail(std::string email);
        void setRegisterData(std::string registerdata);
        void setActive(std::string active);
        void setTwitter(bool twt);
        void keepAlive();

        void setStatus(statusID status);
        void setConnectionID(int id);

        User();
        User(int _ID, string _username, string _nickname, string _password, string _email, string _registerdata, bool twitter);

    private:
        int ID;
        int connectionID;
        sf::Clock connectionClock;
        std::string username;
        std::string nickname;
        std::string password;
        std::string email;
        std::string registerdata;
        std::string active;
        bool twitter;
        statusID status;
};

#endif // _USER
