#ifndef _SERVERDAO
#define _SERVERDAO

#include "configurationDAO.h"
#include "connectionHandler.h"

class ServerDAO{
    public:
        static void logHistory  (string action, string playerName);
        static void logHistory  (string action, string playerName, int playerID);
        static void logPacket   (sf::Packet packet, string inout, int connectionID);
        static void logCrash    (string cause);

};

#endif // _USERDAO

