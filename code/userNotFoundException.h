#ifndef _USRNOTFNDEXP
#define _USRNOTFNDEXP
#include "exceptions.h"


class UserNotFoundException: runtime_error {
    public:

        UserNotFoundException(int _ID) : runtime_error( "User not found" ), id( _ID ), name(""){}

        UserNotFoundException(string _name) : runtime_error( "User not found" ), id(-1), name( _name ){}

        virtual const char* what() const throw() {
            return runtime_error::what();
        }


        int getID() const {
            return id;
        }

    private:
        int id;
        string name;
};

#endif
