#include "serverControl.h"
#include "exceptions.h"

int ServerControl::numberOfOnlineUsers = 0;


int ServerControl::getNumberOfOnlineUsers(){
    int number = 0;
    for(auto& user : UserFacade::getAllUsers()){
        if(user.getStatus() != statusID::offline){
            number++;
        }
    }
    return number;
}

void ServerControl::broadcast(std::string message){
    sf::Packet packet;
    Utility::buildPacketHeader(-1, packetID::Broadcast, packet);
    packet << message;

    for(auto& user : connectionHandler.connections){
        user->send(packet);
    }
}

void ServerControl::sendUsersList(){
    sf::Packet packet;
    Utility::buildPacketHeader(-1, packetID::UserList, packet);

    packet << static_cast<int> ( getNumberOfOnlineUsers() );

    for(auto& user : UserFacade::getAllUsers()){
        if(user.getStatus() != statusID::offline){
            packet << user.getNickname() << static_cast<int> ( user.getStatus() );
        }
    }

    for(auto& connected : connectionHandler.connections){
        connected->send(packet);
    }
}



void ServerControl::sendPlayersList(int gameMode, int gameTime){
    sf::Packet packet;
    Utility::buildPacketHeader(-1, packetID::PlayerList, packet);

    string code = Utility::toString(gameMode) + Utility::toString(gameTime);
    vector<Player*> rank = gameControl.getRank(gameMode, gameTime);

    packet << code << static_cast<int> ( rank.size() );

    for(auto& player : rank ){
        packet  << player->getNickname()
                << static_cast<int>(player->getElo(gameMode, gameTime))
                << static_cast<int>(player->getVictories(gameMode, gameTime))
                << static_cast<int>(player->getDefeats(gameMode, gameTime))
                << static_cast<int>(player->getDraws(gameMode, gameTime));
    }
    for(auto& connected : connectionHandler.connections){
        connected->send(packet);
    }
}


void ServerControl::sendMatchesList(){
    sf::Packet packet;
    Utility::buildPacketHeader(-1, packetID::MatchList, packet);

    packet << static_cast<int> ( GameFacade::getNumberOfMatches() );
    vector <Match> matches = GameFacade::getMatches();
    for(auto& match : matches){
       packet   << static_cast<int> (match.getID())
                << match.getWhite()->getNickname()
                << match.getBlack()->getNickname()
                << static_cast<int> (match.getGameMode())
                << static_cast<int> (match.getGameTime())
                << static_cast<int> (match.getPublic())
                << static_cast<int> (match.getRanked())
                << static_cast<int> (match.getStatus());
    }

    for(auto& connected : connectionHandler.connections){
        connected->send(packet);
    }
}


bool ServerControl::processLogin(int connectionID, std::string username, std::string password, std::string version){
    sf::Packet packet;

    if(version == GAME_VERSION){

        Utility::buildPacketHeader(connectionID, packetID::LoginResponse, packet);

        if(UserFacade::findUser(username)){

            if(UserFacade::getUser(username)->getPassword() == password){

                User* user = UserFacade::getUser(username);

                if( connectUser( user->getID() ) ){

                    UserFacade::setUserConnectionID(user->getID(), connectionID);

                    ServerDAO::logHistory ("logon", username, user->getConnectionID());

                    twitterModule.sendTweet(user->getNickname() + " now online!");

                    user->keepAlive();

                    packet << true << user->getNickname();
                    Player* player = GameFacade::getPlayer(user->getID());
                    for(int k = 0; k < 3; k++){
                        for(int j = 0; j < 3; j++){
                            packet << player->getElo(k, j);
                        }
                    }
                   // packet << GameFacade::getPlayer(UserFacade::getUser(username)->getID())->getElos();
                }
                else{
                    ServerDAO::logHistory ("already connected", username, connectionID);
                    Utility::buildPacketHeader(connectionID, packetID::AlreadyConnected, packet);
                }
            }
            else{
                ServerDAO::logHistory ("incorrect password", username, connectionID);
                packet << false;
            }
        }
        else{
            ServerDAO::logHistory ("not found", username, connectionID);
            packet << false;
        }
    }
    else{
        Utility::buildPacketHeader(connectionID, packetID::WrongVersion, packet);
    }

    Utility::sendPacket(connectionID, packet);

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            sendPlayersList(i, j);
        }
    }

}


void ServerControl::keepUserAlive(int connectionID){
    User* user = UserFacade::getUserByConnection(connectionID);
    if(user != nullptr){
        user->keepAlive();
    }
}


bool ServerControl::connectUser(int ID){
    if(UserFacade::setUserStatus(ID, statusID::online)){
        connectionHandler.lastLogin = ID;
        numberOfOnlineUsers++;
        return true;
    }
    else return false;
}


bool ServerControl::disconnectUser(int ID){
    if(UserFacade::setUserStatus(ID, statusID::offline)){
        numberOfOnlineUsers--;
        User* user = UserFacade::getUser(ID);
        connectionHandler.lastLogout = ID;
        ServerDAO::logHistory ("disconnected", user->getUsername(), user->getConnectionID());
        user->setConnectionID(-1);
        return true;
    }
    else return false;
}

void ServerControl::disconnectOfflineUsers(){
    for(auto& user : UserFacade::getAllUsers()){
        if(user.getStatus() != statusID::offline and !user.isAlive()){
            disconnectUser(user.getID());
        }
    }
}
