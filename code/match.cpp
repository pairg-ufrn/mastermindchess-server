#include "match.h"

Match::Match(int ID, Player* white, Player* black, int gameMode, int gameTime, int isPublic, int ranked){
    this->ID = ID;
    this->white = white;
    this->black = black;
    this->gameMode = gameMode;
    this->gameTime = gameTime;
    this->isPublic = isPublic;
    this->ranked = ranked;

    this->startTime = Utility::getDatestamp(false, true);
    numberWatching = 0;
    status = 1;
    this->matchHistory = "";
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 10; j++){
            board[i][j] = " ";
        }
    }
}

Match::~Match(){

}


Player* Match::getWhite(){
    return this->white;
}


Player* Match::getBlack(){
    return this->black;
}


int Match::getGameMode(){
    return this->gameMode;
}


int Match::getGameTime(){
    return this->gameTime;
}

int Match::getID(){
    return this->ID;
}

string Match::getMatchHistory(){
    return this->matchHistory;
}


vector<int> Match::getSpectators(){
    return this->spectators;
}

int Match::getEloWon(){
    return this->eloWon;
}

int Match::getEloLost(){
    return this->eloLost;
}

Player* Match::getLoser(){
    return this->loser;
}

Player* Match::getWinner(){
    return this->winner;
}

int Match::getRanked(){
    return this->ranked;
}

int Match::getStatus(){
    return this->status;
}

int Match::getPublic(){
    return this->isPublic;
}

string Match::getBoard(int i, int j){
    return board[i][j];
}


void Match::setWinner(Player* winner){
    this->winner = winner;
}

void Match::setLoser(Player* loser){
    this->loser = loser;
}

void Match::setEloWon(int eloWon){
    this->eloWon = eloWon;
}

void Match::setEloLost(int eloLost){
    this->eloLost = eloLost;
}

void Match::setBoard(string newBoard[8][10]){
    int i, j;
    for(i = 0; i < 8; i++){
        for(j = 0; j < 10; j++){
            board[i][j] = newBoard[i][j];
        }
    }
}

void Match::setHistory(string history){
    matchHistory = history;
}

void Match::insertSpectator(int playerID){
    spectators.push_back(playerID);
}
