#include "rankManager.h"

using namespace std;

RankManager::RankManager(){


    for(int i = 0; i < 3; i++){
        classicRank.push_back(vector<Player*>());
        fischerRank.push_back(vector<Player*>());
        capaRank.push_back(vector<Player*>());
    }
    Rank.push_back(classicRank);
    Rank.push_back(fischerRank);
    Rank.push_back(capaRank);

}

void RankManager::loadRanks(vector<Player>& players){
    int i, j;
    playersReference.clear();

    for(i = 0; i < players.size(); i++){
        playersReference.push_back(&players[i]);
    }

    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
            Rank[i][j].clear();
            sortRank(i, j);
        }
    }
}


void RankManager::sortRank(int gameMode, int gameTime){
    int i;
    for(i = 0; i < playersReference.size(); i++){
        rankPlayer(playersReference[i], gameMode, gameTime);
    }
}


void RankManager::rankPlayer(Player* player, int gameMode, int gameTime){
    int eloScore = player->getElo(gameMode, gameTime);
    int position = Rank[gameMode][gameTime].size();

    int i = position;

    Rank[gameMode][gameTime].push_back(player);

    while(i - 1 >= 0 and eloScore > Rank[gameMode][gameTime][i - 1]->getElo(gameMode, gameTime)){ //iterate through the rank
        std::iter_swap(Rank[gameMode][gameTime].begin() + i, Rank[gameMode][gameTime].begin() + i - 1);
        i--;
    }
}

void RankManager::updatePlayerRank(Player* player, int gameMode, int gameTime){
    int eloScore = player->getElo(gameMode, gameTime);
    int i = getPlayerRank(player, gameMode, gameTime);
    int size = Rank[gameMode][gameTime].size();

    while(i - 1 >= 0 and eloScore > Rank[gameMode][gameTime][i - 1]->getElo(gameMode, gameTime)){
        std::iter_swap(Rank[gameMode][gameTime].begin() + i, Rank[gameMode][gameTime].begin() + i - 1);
        i--;
    }

    while(i + 1 < size and eloScore < Rank[gameMode][gameTime][i + 1]->getElo(gameMode, gameTime)){
        std::iter_swap(Rank[gameMode][gameTime].begin() + i, Rank[gameMode][gameTime].begin() + i + 1);
        i++;
    }

}

void RankManager::addNewPlayerToRank(Player* player){
    int i, j;
    playersReference.push_back(player);
    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
            rankPlayer(player, i, j);
        }
    }

}

void RankManager::updatePlayersReference(vector<Player>& players){
    int i;
    playersReference.clear();

    for(i = 0; i < players.size(); i++){
        playersReference.push_back(&players[i]);
    }
}

int RankManager::getPlayerRank(Player* player, int gameMode, int gameTime){
    int i;
    int position = -1;
    int size = Rank[gameMode][gameTime].size();
    string nick = player->getNickname();

    for(i = 0; i < size; i++){
        if(Rank[gameMode][gameTime][i]->getNickname() == nick){
            position = i;
        }
    }
    return position;
}

vector<Player*> RankManager::getRank(int gameMode, int gameTime){
    return Rank[gameMode][gameTime];
}
